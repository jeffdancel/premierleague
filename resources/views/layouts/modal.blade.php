        <!-- /.row -->

@if (Session::has('response'))

        <!-- Success response -->

        <div id="responseModal" class="modal fade" role="dialog">

          <div class="modal-dialog">

            <!-- Modal content-->

            <div class="modal-content">

              <div class="modal-header" style="padding: 5px;">

                <h4 class="modal-title">Message</h4>
                
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              
              </div>

              <div class="modal-body">

                <p style = "font-size: 30px;">{!! Session::get('response')['response_message']!!} </p>

              </div>

              <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

              </div>

            </div>

          </div>

        </div>

        <!-- End -->


        <script>

        $(document).ready(function(){

        $('#responseModal').modal('show');

        })

        </script>
@endif
