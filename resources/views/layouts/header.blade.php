
<nav  class="navbar navbar-inverse navbar-fixed-top" style="background: #37003c;top: 20px;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                <div class="navbar-header">
      <a class="navbar-brand" href="#"><img src="{{ asset('public/assets/logo/pl-logo.png') }}" style="border-radius: 50%;margin-top: -40px;width: 80%;"> </a>
    </div>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
             <ul class="nav navbar-nav">
              <li><a href="{{ route('premierleague.home') }}">Home</a></li>
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Players
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ route('players.list') }}">List of Players</a></li>
                  <li><a href="javascript:void(0)" id="fetch-player-btn">Fetch players API</a></li>
                  <li><a href="{{ route('players.id.name') }}" target="_blank">API get players ID and Name</a></li>
                </ul>
              </li>
            </ul>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<script type="text/javascript">
  $(document).ready(function(){
    // display loading screen while back end process is on going
    $("#fetch-player-btn").on("click",function(){
      $(".loader-container").removeClass("display-none");
      window.location = "{{ route('players.fetch') }}";
    })

  })
</script>
