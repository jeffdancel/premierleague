<!DOCTYPE html>
<html>
	<head>
		<title>{{ $title }}</title>
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('public/assets/logo/favicon.ico') }}">
		<link href="https://fonts.googleapis.com/css?family=Comfortaa:400,700&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
	  	<link rel="stylesheet" href="{{ asset('public/assets/css/_common.css') }}">
		<link rel="stylesheet" href="{{ asset('public/assets/bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

	</head>
	<body>
	 	<!-- Header content -->
    	@include("layouts.header")
    		
			@yield('content')

		<!-- Modals -->
		@include("layouts.loading")	

		<!-- Modals -->
		@include("layouts.modal")	

	</body>
</html>