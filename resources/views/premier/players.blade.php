@extends('layouts.main')
@section('content') 
<div class = "container-fluid page-container" style="margin-top: 100px;">
	<div class = "row">
		<div class = "col-md-12">
			<div class = "col-md-12">
				<h2>Players</h2>
			</div>
			<div class = "col-md-12">
                <table width="100%" class="table" id="dataTables-users">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Ponts Per Game</th>
                            <th>Total Points</th>
                            <th>Action</th>
                        </tr>

                    </thead>

                    <tbody>

                    	<!-- Data via api -->

                    </tbody>

                </table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

	$(document).ready(function(){

		$('#dataTables-users').DataTable({
                 "columnDefs": [
                    { "orderable": false, "targets": 4 }
                  ],
				"ordering": true,
                "processing": true,
                "serverSide":true,
                "ajax":{
                    url:"{{ route('players.ajax.list') }}",
                    type:"get",
                }
        });
	})

</script>
@endsection 