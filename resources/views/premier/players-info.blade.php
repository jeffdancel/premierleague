@extends('layouts.main')
@section('content') 
<div class = "container-fluid page-container" style="margin-top: 100px;">
	<div class = "col-md-12">
        <h2>Players Information</h2>
    </div>

    <ul class="breadcrumb">
      <li><a href="{{ route('players.list') }}">Players</a></li>
      <li>Players information</li>
    </ul>

    <div class = "row">
		<div class = "player-info-container col-sm-6">
            <div class="form-group">
                <label>ID</label>
                <span>{{ $player->id }}</span>
            </div>
            <div class="form-group">
                <label>Name</label>
                <span>{{ $player->first_name }}{{ $player->second_name }}</span>
            </div>
            <div class="form-group">
                <label>Chance of playing next round</label>
                <span>{{ $player->chance_of_playing_next_round }}</span>
            </div>
            <div class="form-group">
                <label>Chance of playing this round</label>
                <span>{{ $player->chance_of_playing_this_round }}</span>
            </div>
            <div class="form-group">
                <label>code</label>
                <span>{{ $player->code }}</span>
            </div>
            <div class="form-group">
                <label>Cost change event</label>
                <span>{{ $player->cost_change_event }}</span>
            </div>
            <div class="form-group">
                <label>Cost change event fall</label>
                <span>{{ $player->cost_change_event_fall }}</span>
            </div>
            <div class="form-group">
                <label>Cost change start</label>
                <span>{{ $player->cost_change_start }}</span>
            </div>
            <div class="form-group">
                <label>Cost change start fall</label>
                <span>{{ $player->cost_change_start_fall }}</span>
            </div>
            <div class="form-group">
                <label>Dreamteam count</label>
                <span>{{ $player->dream_count }}</span>
            </div>
            <div class="form-group">
                <label>Element type</label>
                <span>{{ $player->element_type }}</span>
            </div>
            <div class="form-group">
                <label>Ep next</label>
                <span>{{ $player->ep_next }}</span>
            </div>
            <div class="form-group">
                <label>Ep this</label>
                <span>{{ $player->ep_this }}</span>
            </div>
            <div class="form-group">
                <label>Event points</label>
                <span>{{ $player->event_points }}</span>
            </div>
            <div class="form-group">
                <label>Form</label>
                <span>{{ $player->form }}</span>
            </div>
            <div class="form-group">
                <label>In dreamteam</label>
                <span>{{ $player->in_dreamteam }}</span>
            </div>
            <div class="form-group">
                <label>News</label>
                <span>{{ $player->news }}</span>
            </div>
            <div class="form-group">
                <label>News added</label>
                <span>{{ $player->news_added }}</span>
            </div>
            <div class="form-group">
                <label>Now cost</label>
                <span>{{ $player->now_cost }}</span>
            </div>
            <div class="form-group">
                <label>Photo</label>
                <span>{{ $player->photo }}</span>
            </div>
            <div class="form-group">
                <label>Points per game</label>
                <span>{{ $player->points_per_game }}</span>
            </div>
            <div class="form-group">
                <label>Selected by percent</label>
                <span>{{ $player->selected_by_percent }}</span>
            </div>    
            <div class="form-group">
                <label>Special</label>
                <span>{{ $player->special }}</span>
            </div>
            <div class="form-group">
                <label>Squad_number</label>
                <span>{{ $player->squad_number }}</span>
            </div>
            <div class="form-group">
                <label>Status</label>
                <span>{{ $player->status }}</span>
            </div>                        
        </div>
        <div class = "player-info-container col-sm-6" >
            <div class="form-group">
                <label>Team</label>
                <span>{{ $player->team }}</span>
            </div>
            <div class="form-group">
                <label>Team Code</label>
                <span>{{ $player->team_code }}</span>
            </div>
            <div class="form-group">
                <label>Total Points</label>
                <span>{{ $player->total_points }}</span>
            </div>
            <div class="form-group">
                <label>Transfers in</label>
                <span>{{ $player->transfers_in }}</span>
            </div>
            <div class="form-group">
                <label>Transfers out</label>
                <span>{{ $player->transfers_out }}</span>
            </div>
            <div class="form-group">
                <label>Value form</label>
                <span>{{ $player->value_form }}</span>
            </div>
            <div class="form-group">
                <label>Value season</label>
                <span>{{ $player->value_season }}</span>
            </div>
            <div class="form-group">
                <label>Web name</label>
                <span>{{ $player->web_name }}</span>
            </div>
            <div class="form-group">
                <label>Minutes</label>
                <span>{{ $player->minutes }}</span>
            </div>
            <div class="form-group">
                <label>Goals scored</label>
                <span>{{ $player->goals_scored }}</span>
            </div>
            <div class="form-group">
                <label>Assists</label>
                <span>{{ $player->assists }}</span>
            </div>
            <div class="form-group">
                <label>Clean sheets</label>
                <span>{{ $player->clean_sheets }}</span>
            </div>
            <div class="form-group">
                <label>Goals conceded</label>
                <span>{{ $player->goals_conceded }}</span>
            </div>
            <div class="form-group">
                <label>Own goals</label>
                <span>{{ $player->own_goals }}</span>
            </div>
            <div class="form-group">
                <label>Penalties saved</label>
                <span>{{ $player->penalties_saved }}</span>
            </div>
            <div class="form-group">
                <label>Penalties missed</label>
                <span>{{ $player->penalties_missed }}</span>
            </div>
            <div class="form-group">
                <label>Yellow cards</label>
                <span>{{ $player->yellow_cards }}</span>
            </div>
            <div class="form-group">
                <label>Red cards</label>
                <span>{{ $player->red_cards }}</span>
            </div>
            <div class="form-group">
                <label>Saves</label>
                <span>{{ $player->saves }}</span>
            </div>
            <div class="form-group">
                <label>Bonus</label>
                <span>{{ $player->bonus }}</span>
            </div>
            <div class="form-group">
                <label>Bps</label>
                <span>{{ $player->bps }}</span>
            </div>
            <div class="form-group">
                <label>Influence</label>
                <span>{{ $player->influence }}</span>
            </div>
            <div class="form-group">
                <label>Creativity</label>
                <span>{{ $player->creativity }}</span>
            </div>
            <div class="form-group">
                <label>Threat</label>
                <span>{{ $player->threat }}</span>
            </div>
            <div class="form-group">
                <label>Ict index</label>
                <span>{{ $player->ict_index }}</span>
            </div>
        </div>
	</div>
</div>
@endsection