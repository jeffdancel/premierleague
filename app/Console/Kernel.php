<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\Players;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        $schedule->call(function () { 
            try {
                $players = new Players();
                $playersDataApi = json_decode(file_get_contents("https://fantasy.premierleague.com/api/bootstrap-static/"));
                $ctr = 0;
                // used to check if the provided data players trait 
                // is matched in our database
                $playerColumn = $players->checkIfColumnExist($playersDataApi->elements[1]);
                if($playerColumn["status"] == 0) {
                    $ctr=0;
                    $lastPlayerID = Players::getLastPlayerID();

                    foreach($playersDataApi->elements as $element => $object) {
                        $arrData = array();
                        $player_exist = false;
                        // check if player id exist to avoid duplicate entries
                        $player_exist = $players->checkPlayerExist($object->id);
                        if($ctr <= 100){
                            if(!$player_exist) {
                                foreach ($object as $key => $value) {
                                    $arrData[$key] = $value;
                                }
                            Players::insert([$arrData]);
                            $ctr++;
                        }
                    }
                }
              }
            } catch(Exception $e) {
              // do nothing
            }
        })->everyThirtyMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
