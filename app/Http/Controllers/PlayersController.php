<?php

namespace App\Http\Controllers;
use App\Models\Players;
use Illuminate\Http\Request;
use Session;
use Redirect;
use View;

class PlayersController extends Controller
{
	// get all players
	public function playersList() {
		$data["title"] = "Players | Premier";
		return view('premier.players')->with($data);
	}

	// fetch data for datatables
	public function getPlayersListDataTables(Request $request) {
		$player = new Players;
    	$result = $player->getPlayerList($request->all());

        $data['draw'] = $request->draw;
        $data['recordsFiltered'] = $result['recordsFilter'];
        $data['recordsTotal'] = $result['recordsTotal'];
        $data['data'] = $result['data'];

        return  response()->json($data, 200, ['Access-Control-Allow-Origin' => '*']);
	}

	// get players details limit to ID and Name
	public function getPlayersApiIdName() {
		$players = new Players();
		$result = $players->getPlayersNameId();
		echo json_encode($result);
	}

	public function getPlayersDetail($id=null) {
		$data["title"] = "Players | Information";
		$players = new Players();
		$data["player"] = $players->getPlayersDetailApi($id);
		if(empty($data["player"])) {
			$response["response_message"] = "Player requested doesn't exist";
	  		
            Session::flash('response',$response);
            return Redirect::to('/players/list');
		}

		return view('premier.players-info')->with($data);
	}

	// show players information using json object
	public function getPlayersDetailApi($id=null) {
		$players = new Players();
		$result = $players->getPlayersDetailApi($id);
		if(empty($result)) {
			$result = "Player requested doesn't exist";	
		}
		echo json_encode($result);
	}

    //** Fetch players from a provided data and store 
    //store it in the database** 
    public function fetchPlayers() {  
    	try {
    		$players = new Players;
			$playersDataApi = json_decode(file_get_contents("https://fantasy.premierleague.com/api/bootstrap-static/"));
	  		$ctr = 0;
		  // used to check if the provided data players trait 
		  // is matched in our database
	  		$playerColumn = $players->checkIfColumnExist($playersDataApi->elements[1]);
			  if($playerColumn["status"] == 0) {
			  	$ctr=0;
			  	$lastPlayerID = Players::getLastPlayerID();

			  	foreach($playersDataApi->elements as $element => $object) {
			  		$arrData = array();
			  		$player_exist = false;
			  		// check if player id exist to avoid duplicate entries

		  			$player_exist = $players->checkPlayerExist($object->id);
		  			if($ctr <= 100){
		  				if(!$player_exist) {
				  		foreach ($object as $key => $value) {
				  				$arrData[$key] = $value;
				  			}
				  			Players::insert([$arrData]);
				  			$ctr++;
				  		}
				  	}
	  			}
		  		if ($ctr>0) {
		  			$response["response_message"] = "You have successfully stored 100 new players";
		  		} else {
		  			$response["response_message"] = "No new players added";
		  		}
		  		
	            Session::flash('response',$response);
	            return Redirect::to('/players/list');

			  } else { 
			  		echo "This ". $playerColumn["column"] . "doesnt exist on the database"; 
			  }
			} catch(Exception $e) {
			  echo 'Message: ' .$e->getMessage();
			}
    }
}
