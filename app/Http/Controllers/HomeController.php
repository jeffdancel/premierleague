<?php

namespace App\Http\Controllers;
use App\Models\Players;
use Illuminate\Http\Request;

class  HomeController extends Controller
{
	public function index() {
		$data["title"] = "Home | Premier League";
		return view('premier.home')->with($data);
	}
}
