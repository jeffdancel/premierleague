<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Players extends Model
{   
	protected $table = 'players';
  	protected $primaryKey = 'id';

    //get all player list
    public function getPlayerList($request)
    {
        $result['data'] = [];
        $select = [];
        $search = $request["search"]['value'];

        $classData = new self;
        $query = $classData->select(
          DB::raw('
            players.id as id,
            players.first_name as first_name,
            players.second_name as second_name,
            players.points_per_game as points_per_game,
            players.total_points as total_points')
        );

        if(!empty($search)) {
            $query->where(function ($subquery) use ($search) {
                $subquery->where('players.first_name', 'like', $search.'%')
                            ->orWhere('players.first_name', 'LIKE', '%'.$search.'%')
                            ->orWhere('players.second_name', 'LIKE', '%'.$search.'%')
                            ->orWhere('players.points_per_game', 'LIKE', '%'.$search.'%')
                            ->orWhere('players.total_points', 'LIKE', '%'.$search.'%');
              });  
        }   

        $result['recordsFilter'] = count($query->get());
        $result['recordsTotal'] = count($query->get());

        if ($request["order"][0]['column'] < 5) {
            $order = ['players.id',
                    'players.first_name',
                    'players.points_per_game',
                    'players.total_points',
            ];

            $query->orderBy($order[$request["order"][0]['column']], $request["order"][0]['dir']);
        } else {
            $query->orderBy('players.id','asc');
        }

        $query->skip($request["start"])->limit($request["length"]);

        $data = $query->get();
    
        foreach ($data as $player) {
            $result['data'][] = array(
            $player['id'],
            "<a href = '".route('players.view',$player->id)."' >".$player['first_name']. " " .$player['second_name'] ."</a>",
            $player['points_per_game'],
            $player['total_points'],
            "<a href='".route('players.details.api',$player->id)."' target='_blank'>Check player details through API</a>"
            
          );
        }

        return $result;
    }

    public function getPlayersNameId() {
        $players = self::select('id', 'first_name','second_name')->get();
        return $players;
    }

    public function getPlayersDetailApi($id=null) {
        $players = self::select('*')
                        ->where('id',"=",$id)
                        ->first();
        return $players;
    }

	// get player table columns
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

     //get the last row to avoid duplicate entry
    public static function getLastPlayerID() {
    	  $last_record = DB::table('players')->orderBy('id', 'DESC')->first();
        if(!empty($last_record)) {
            return $last_record->id;    
        } else {
            return 1;
        }
    }

    // Check if columns passed by data provided are matched with the database
    public function checkIfColumnExist($data){
        $traitCtr = 0;
        $traitCatch="";
        $players = new Players;
        $playerTraits = $players->getTableColumns();
        foreach($data as $key => $value) {
            if (in_array($key, $playerTraits)) {
                //do nothing
                // continue on fetchplayers function
            } else {
                $traitCatch=$key;
                $traitCtr++;
            }
        }
        $response_data = array("status"=>0,"column"=>$key);
        return $response_data;
    }

    public function checkPlayerExist($id){
        $player = Players::find($id);
        if(!empty($player)) {
            return true;
        } else {
            return false;
        }
    }
}