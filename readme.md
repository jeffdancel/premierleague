# README #

Hello this is my web application for your code challenge.

### How do I get set up? ###

PHP version must be 7.2 and up.

Database name is premierleague_db
Database is located at the database folder under root folder.

No need to do hosts file editing the app will run as soon as the database has been imported.

### Importer ###
importer is set up both programatically and manually.
the method is on the kernel which will trigger every 30 minutes.
Kindly put this script on your cron job machine 
	> * * * * * php /premierleague/artisan schedule:run >> /dev/null 2>&1
And you can also do it manually by clicking the "fetch player api" under the player menu header


Thank you :)