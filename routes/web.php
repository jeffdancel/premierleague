<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [ 'as' => 'premierleague.home',
				 'uses' => 'HomeController@index']);

Route::get('/players/list',[ 'as' => 'players.list',
							 'uses' => 'PlayersController@playersList']);

Route::get('/players.ajax.list',[ 'as' => 'players.ajax.list',
								 'uses' => 'PlayersController@getPlayersListDataTables']);

Route::get('/fetch-players',[ 'as' => 'players.fetch', 'uses' => 'PlayersController@fetchPlayers']);

Route::get('/players/id-name',[ 'as' => 'players.id.name',
							 'uses' => 'PlayersController@getPlayersApiIdName']);

Route::get('/players/view/{id}',[ 'as' => 'players.view',
							 'uses' => 'PlayersController@getPlayersDetail']);

Route::get('/players/detail-api/{id}',[ 'as' => 'players.details.api',
							 'uses' => 'PlayersController@getPlayersDetailApi']);